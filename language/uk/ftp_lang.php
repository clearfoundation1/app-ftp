<?php

$lang['ftp_app_description'] = 'FTP-сервер забезпечує управління файлами та їх зберігання за допомогою стандартних протоколів FTP та FTP Secure.';
$lang['ftp_app_name'] = 'FTP-сервер';
$lang['ftp_failed_logins'] = 'Помилка входу на FTP';
$lang['ftp_flexshare_port'] = 'Flexshare порт';
$lang['ftp_home_share_port'] = 'Порт домашньої шари';
$lang['ftp_max_instances'] = 'Максимальна кількість підключень';
$lang['ftp_maximum_instances_invalid'] = 'Максимальна кількість підключень недійсна.';
$lang['ftp_port_reserved_for_flexshare'] = 'Порт зарезервований для Flexshare.';
$lang['ftp_server_name'] = 'Ім`я сервера';
$lang['ftp_server_name_invalid'] = 'Неприпустиме ім`я сервера.';
